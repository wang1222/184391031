 Docker容器管理
1.docker容器命令
（1）docker run 命令
 语法： docker run [options]镜像名
 作用：用来创建或者运行容器
      参数：
-i 表示创建要给交互式容器
-t表示运行容器的同时创建一个伪终端，常与 -i 搭配使用
–name 自定义容器名
实例：
[root@master ~]# docker run -d -p 5000:5000 --restart=always --name registry docker.io/registry:latest
4669b2a5fe03207f0f213f7e886906b88f430e5369c9fbbc250a01348ca35a38
（2）docker rm命令
语法：docker rm [options] 容器ID
作用：用来删除一个容器
参数：
-f 表示强制删除
实例：
[root@master ~]# docker ps -a
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
4669b2a5fe03        registry:latest     "/entrypoint.sh /etc…"   6 minutes ago       Up 6 minutes        0.0.0.0:5000->5000/tcp   registry
[root@master ~]# docker rm -f 4669b2a5fe03
4669b2a5fe03
[root@master ~]# docker ps -a
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
（3）docker ps命令
语法：docker ps  [options] 
作用：用来查看当前正在运行的容器对象
参数：
-l 默认的查看只会查看正在运行中的容器信息
-a 显示所有运行过的镜像信息
-q 表示只显示对应的容器id 信息
 实例：
[root@master ~]# docker ps -l
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
0b9233a7f0cc        registry:latest     "/entrypoint.sh /etc…"   2 seconds ago       Up 1 second         0.0.0.0:5000->5000/tcp   registry
[root@master ~]# docker ps -a
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
0b9233a7f0cc        registry:latest     "/entrypoint.sh /etc…"   8 seconds ago       Up 7 seconds        0.0.0.0:5000->5000/tcp   registry
[root@master ~]# docker ps -q
0b9233a7f0cc
（4）docker restart命令
语法：docker  restart  容器ID 
作用：用来重启一个容器
实例：
[root@master ~]# docker ps -a
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
ae35b6a6c168        registry:latest     "/entrypoint.sh /etc…"   6 seconds ago       Up 5 seconds        0.0.0.0:5000->5000/tcp   registry
[root@master ~]# docker restart ae35b6a6c168
ae35b6a6c168
（5）docker stop命令
语法：docker  stop  容器ID 
作用：用来停止一个容器，这种停止方法属于慢动作停止，类似于电脑关机
实例：
[root@master ~]# docker stop ae35b6a6c168
ae35b6a6c168
（6）doker kill命令
 语法：docker kill 容器ID 
作用：也是用来停止一个容器，但这种停止方法属于直接结束线程，类似于拔电源操作
实例：
[root@master ~]#docker kill ae35b6a6c168
ae35b6a6c168
（7）docker logs命令
 语法：docker logs 容器ID 
作用：用来查看容器日志
参数：
-f 跟随打印最新的日志追加在最后面
-t 显示日志打印的时间戳
–tail 显示最新的指定数量的几条日志信息
实例：
[root@master ~]#docker logs -f 25560379103e
time="2020-05-12T15:21:31.439410581Z" level=info msg="Starting upload purge in 52m0s" go.version=go1.11.2 instance.id=6e51e7c8-623e-4586-a544-c056d349d005 service=registry version=v2.7.1 
......以下信息省略......
    [root@master ~]#docker logs -t 25560379103e
2020-05-12T15:21:31.439868603Z time="2020-05-12T15:21:31.439410581Z" level=info msg="Starting upload purge in 52m0s" go.version=go1.11.2 instance.id=6e51e7c8-623e-4586-a544-c056d349d005 service=registry version=v2.7.1
                     ......以下信息省略......
[root@master ~]# docker logs --tail 1 25560379103e
time="2020-05-12T15:24:14.054498391Z" level=info msg="listening on [::]:5000" go.version=go1.11.2 instance.id=97d81e2b-757b-447b-8985-b8c8dc6c755c service=registry version=v2.7.1
......以下信息省略......
（8）docker top命令
 语法：docker top 容器ID 
作用：用来查看容器内运行的线程信息
实例：
[root@master ~]# docker top 25560379103e
UID                 PID                 PPID                C                   STIME               TTY                 TIME                CMD
root                12855               12838               0                   11:24               ?                   00:00:00            registry serve /etc/docker/registry/config.yml
（9）docker inspect命令
语法：docker inspect 容器ID 
作用：用来查看容器内部细节信息，是一个json串
实例：
[root@master ~]# docker inspect 25560379103e
[
    {
        "Id": "25560379103e1849456646a83a76fff71b3936ddb5e8760f28c6414940a94a74",
        "Created": "2020-05-12T15:21:30.987768102Z",
        "Path": "/entrypoint.sh",
        "Args": [
            "/etc/docker/registry/config.yml"
        ]
......以下信息省略......
（10）docker export命令
语法：docker export 容器ID  > tar包
作用：导出容器
实例：
[root@master ~]# docker ps -a
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
25560379103e        registry:latest     "/entrypoint.sh /etc…"   34 minutes ago      Up 31 minutes       0.0.0.0:5000->5000/tcp   registry
[root@master ~]# docker export 25560379103e > test.tar
（11）docker import命令
作用：导入容器
实例：
[root@master ~]# cat test.tar | docker import - test/ubuntu:v1.0
sha256:3836ae16e71e51901517e21f9fc698f5473ef61ece742695297fa68658be03d2
[root@master ~]# docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
test/ubuntu         v1.0                3836ae16e71e        13 seconds ago      25.7MB
httpd               latest              d3017f59d5e2        6 months ago        165MB
busybox             latest              020584afccce        6 months ago        1.22MB
nginx               latest              540a289bab6c        6 months ago        126MB