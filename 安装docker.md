
（1）将提供的压缩包Docker.tar.gz上传至/root目录并解压。

[root@master ~]# tar -zxvf Docker.tar.gz

（2）配置本地YUM源

[root@master ~]# mv /etc/yum.repos.d/* /media/

[root@master ~]# vi /etc/yum.repos.d/local.repo 

[centos]

name=centos

baseurl=file:///root/Docker

gpgcheck=0

enabled=1

[root@master ~]# yum clean all

Loaded plugins: fastestmirror

Cleaning repos: centos

Cleaning up list of fastest mirrors

Other repos take up 39 M of disk space (use --verbose for details)

[root@master ~]# yum repolist

Loaded plugins: fastestmirror

Determining fastest mirrors

centos         | 2.9 kB  00:00:00                                                                             

centos/primary_db    | 695 kB  00:00:00                                                                           

repo id        repo name         status                                                                        

centos         centos          463                                                                     

repolist: 463

（3）升级系统内核

由于内核版本比较低，部分功能（如overlay2存储层驱动）无法使用，并且部分功能可能不太稳定，建议升级内核。

升级系统内核，命令如下，：

[root@master ~]# yum upgrade -y

（4）配置防火墙及SELinux

配置防火墙及SELinux，示例代码如下：

[root@master ~]# systemctl stop firewalld && systemctl disable firewalld

[root@master ~]# iptables -t filter -F

[root@master ~]# iptables -t filter -X

[root@master ~]# sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config

[root@master ~]# reboot

（5）开启路由转发

[root@master ~]# cat >> /etc/sysctl.conf << EOF

\> net.ipv4.ip_forward=1

\> net.bridge.bridge-nf-call-ip6tables = 1

\> net.bridge.bridge-nf-call-iptables = 1

\> EOF

[root@master ~]# modprobe br_netfilter

[root@master ~]# sysctl -p

net.ipv4.ip_forward = 1

net.bridge.bridge-nf-call-ip6tables = 1

net.bridge.bridge-nf-call-iptables = 1

（6）安装依赖包

yum-utils提供了yum-config-manager的依赖包，device-mapper-persistent-data和lvm2are需要devicemapper存储驱动。

[root@master ~]# yum install -y yum-utils device-mapper-persistent-data

（7）安装docker-ce

随着Docker的不断流行与发展，Docker组织也开启了商业化之路，Docker从17.03版本之后分为CE（CommunityEdition）和EE（EnterpriseEdition）两个版本。

Docker EE专为企业的发展和IT团队建立，为企业提供最安全的容器平台，以应用为中心的平台，有专门的团队支持，可在经过认证的操作系统和云提供商中使用，并可运行来自DockerStore的经过认证的容器和插件。

Docker CE是免费的Docker产品的新名称，Docker CE包含了完整的Docker平台，非常适合开发人员和运维团队构建容器APP。

此处安装指定版本的Docker CE。

[root@master ~]# yum install docker-ce-18.09.6 docker-ce-cli-18.09.6 containerd.io -y

（8）启动Docker

启动Docker并设置开机自启。

[root@master ~]# systemctl daemon-reload

[root@master ~]# systemctl restart docker

[root@master ~]# systemctl enable docker

查看Docker的系统信息。

[root@master ~]# docker info

Containers: 0

 Running: 0

 Paused: 0

 Stopped: 0

Images: 0

Server Version: 18.09.6

Storage Driver: devicemapper

至此，Docker引擎的安装就完成了。