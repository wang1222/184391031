 ***\*Compose编排使用\****

（1）安装Docker-Compose

下载Docker-Compose，此处选择1.25.0-rc2版本。可以从网络下载，也可以从提供的光盘包里上传后使用。

 [root@master ~]# curl -L https://github.com/docker/compose/releases/download/1.25.0-rc2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose

 % Total   % Received % Xferd  Average Speed  Time   Time   Time  Current

​                 Dload  Upload  Total  Spent   Left  Speed

100  638  100  638   0   0   806    0 --:--:-- --:--:-- --:--:--  807

100 16.1M  100 16.1M   0   0  127k    0  0:02:09  0:02:09 --:--:-- 87341

（2）赋予可执行权限

[root@master ~]# chmod +x /usr/local/bin/docker-compose

（3）查看版本	

[root@master ~]# docker-compose --version

docker-compose version 1.25.0-rc2, build 661ac20e

（4）创建项目目录

[root@master ~]# mkdir composetest

[root@master ~]# cd composetest

（5）定义app.py文件

在项目路径下创建app.py文件，并将以下内容写入文件。 

 [root@master composetest]# vi  app.py

import time

import redis

from flask import Flask

app = Flask(__name__)

cache = redis.Redis(host='redis', port=6379)

def get_hit_count():

  retries = 5

  while True:

​    try:

​      return cache.incr('hits')

​    except redis.exceptions.ConnectionError as exc:

​      if retries == 0:

​        raise exc

​      retries -= 1

​      time.sleep(0.5)

@app.route('/')

def hello():

  count = get_hit_count()

  return 'Hello World! I have been seen {} times.\n'.format(count)

if __name__ == "__main__":

  app.run(host="0.0.0.0", debug=True)

在这个例子中，Redis就是应用网络中Redis容器的主机名。端口使用的Redis默认端口6379。

（6）定义requirements.txt文件

在项目路径下创建requirements.txt文件，并将以下内容写入文件。

[root@master composetest]# vi requirements.txt

flask

redis 

（7）定义Dockerfile

在这一步中，需要编写一个Dockerfile来构建一个Docker镜像。这个镜像包含Python应用的所有依赖，也包含Python其本身。

在项目路径下创建一个Dockerfile文件，并将以下内容写入文件。

[root@master composetest]# vi Dockerfile

FROM python:3.4-alpine

ADD . /code

WORKDIR /code

RUN pip install -r requirements.txt

CMD ["python", "app.py"] 

本Dockerfile主要完成以下工作：

① 构建一个基于Python 3.4的镜像。

② 把当前目录添加到镜像中的/code路径下。

③ 把工作路径设置成/code。

④ 安装Python依赖。

⑤ 设置容器的默认命令为python app.py。

（8）定义服务

在工作路径下创建一个docker-compose.yml文件并写入以下内容。

[root@master composetest]# vi docker-compose.yml

version: '3'

services:

 web:

  build: .

  ports:

   \- "5000:5000"

 redis:

  image: "redis:alpine" 

这个Compose文件中定义了两个服务Web与Redis。

Web服务使用当前目录Dockerfile构建出来的镜像，并且将容器上暴露的5000端口转发到主机的5000端口，使用Flask Web服务器的默认端口5000。

Redis服务使用从Docker Hub注册表中拉取的公有镜像。

（9）运行服务

在项目路径下通过docker-compose up命令启动应用。

[root@master composetest]# docker-compose up

……

eb_1   |  * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)

web_1   |  * Restarting with stat

web_1   |  * Debugger is active!

web_1   |  * Debugger PIN: 224-122-994

……

（10）克隆一个shell窗口，查看。

[root@master ~]# docker ps -a

CONTAINER ID     IMAGE        COMMAND          CREATED       STATUS             PORTS           NAMES

662634a4bf39     composetest_web   "python app.py"      3 minutes ago    Up 3 minutes          0.0.0.0:5000->5000/tcp  composetest_web_1

9c69d27c2f64     redis:alpine     "docker-entrypoint.s…"  3 minutes ago    Up 3 minutes          6379/tcp         composetest_redis_1

Compose拉取一个Redis镜像，以案例代码构建一个镜像，并启动定义的服务。在这种情况下，代码在构建镜像时被静态拷贝到镜像中。

（11）验证服务

[root@master ~]# curl 127.0.0.1:5000

Hello World! I have been seen 1 times.

[root@master ~]# curl 127.0.0.1:5000

Hello World! I have been seen 2 times.

（12）输入docker image ls命令，列举所有本地镜像，镜像列表中将返回Reidis与Web。

[root@master ~]# docker images

REPOSITORY      TAG         IMAGE ID       CREATED       SIZE

composetest_web   latest        d58fa404496d     About an hour ago  84.5MB

redis        alpine        6f63d037b592     6 months ago     29.3MB